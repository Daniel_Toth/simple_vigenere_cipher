/*
 * Link: https://gitlab.com/Daniel_Toth/simple_vigenere_cipher
 * */

#include <stdio.h>
#include <string.h>

char addchar(char a, char b, int mode) {
	/*Convert case*/
	if (a > 'Z') {
		a = a - ('a'- 'A');
	}
	if (b > 'Z') {
		b = b - ('a'- 'A');
	}
	char c;
	if (mode) {
		//decode
		c = 'A' + (a - 'A') - (b - 'A');
	} else {
		//encode
		c = 'A' + (a - 'A') + (b - 'A');
	}
	if (c > 'Z') {
		c -= 'Z' - 'A' + 1;
	}
	if (c < 'A') {
		c += 'Z' - 'A' + 1;
	}
	return c;
}

int is_letter(char letter) {
	if (((letter >= 'A') && (letter <= 'Z')) || ((letter >= 'a') && (letter <= 'z'))) {
		return 1;
	} else {
		return 0;
	}
}

int main()
{	
	printf("Vigenere cipher\n");
	char a[16384];
	printf("Enter the text that you wish to encode or decode (max 16384 caracters)\n");
	scanf("%[^\n]s", a);
	char pwd[1024];
	printf("Enter password (max 1024 caracters)\n");
	scanf("%s", pwd);
	
	int mode;
	printf("Enter 0 or 1 !\n\tEncode\t0\n\tDecode\t1\n");
	scanf("%d", &mode);
	printf("Result:\n\n");
	int pwdlen = strlen(pwd);
	
	int i = 0, w = 0;
	while (a[i] != '\0') {
		if (is_letter(a[i])) {
			a[i-w] = addchar(a[i], pwd[(i-w) % pwdlen], mode);
			printf("%c", a[i-w]);
		} else {
			if (mode) {
				printf("\nError! The '%c' is an illegal character \n", a[i]);
				return 0;
			}
			w++;
		}
		i++;
	}
	printf("\n\n");	
	return 0;
}
